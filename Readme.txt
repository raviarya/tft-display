Two methods For Installation:

1.You can burn the image file "rpi_35_B_B+_PI2.img" to SD card.

2.Installation drive:

  a. Burn the image file which you make to SD card and start raspberry pi.
  b. Ensure network connected correctly
  c. The LCD screen and raspberry pi development board connected correctly
  d. Copy the drive "LCD_show.tar.gz" to raspberry pi;

     Decompression file      tar -xzvf LCD_show.tar.gz
     Jump into the file      cd  LCD-show
     update system           sudo apt-get update
     Install driver          sudo ./LCD35_v1
     Then after a few seconds, the system will reboot!

   * If you want to use the HDMI, you can run command: sudo./LCD_hdmi

   * When you update the sysytem, you must run command:

     sudo apt-mark hold raspberrypi-bootloader
     sudo apt-get update
     sudo apt-get upgrade




使用方式：

a.最快捷的方式，使用网盘里面的编译好的文件rpi_35_B_B+_PI2.img直接烧录到sd卡。

b.使用网盘里的驱动。

 1.烧写自己的镜像文件到SD卡，并启动Raspberry Pi

 2.保证网络连接正常

 3.将LCD屏与树莓派开发板正确连接

 4.将网盘驱动拷贝到树莓派上（使用ssh或者使用U盘介质挂载）

 5.解压文件并开始安装。

    解压文件               tar -xzvf LCD_show.tar.gz

    跳入文件夹            cd  LCD-show

    先升级更新系统     sudo apt-get update

    安装驱动                sudo ./LCD35_v1

    稍等一段时间系统就会安装驱动并自动重新启动

    如果想重新使用高清屏的话，可以使用 sudo./LCD_hdmi 
    当你更新使用之前，必须使用一下命令 sudo apt-mark hold raspberrypi-bootloader
    然后使用一下命令，
    sudo apt-get update
    sudo apt-get upgrade


